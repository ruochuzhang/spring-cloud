package cn.tedu.rabbitmqspringboot.m1;

import cn.tedu.rabbitmqspringboot.RabbitmqSpringbootApplication;
import org.springframework.amqp.core.Queue;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class Main {
    public static void main(String[] args) {
        SpringApplication.run(Main.class, args);
    }

    @Bean
    public Queue helloworldQueue() {
        //return new Queue("helloworld"); // 持久，非独占，不自动删除
        return new Queue("helloworld", false); //非持久，非独占，不自动删除
    }
}
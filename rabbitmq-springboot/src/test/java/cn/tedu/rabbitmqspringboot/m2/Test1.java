package cn.tedu.rabbitmqspringboot.m2;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Scanner;

@SpringBootTest
public class Test1 {
    @Autowired
    private Producer p;

    @Test
    public void test1() {
        p.send();
    }
}

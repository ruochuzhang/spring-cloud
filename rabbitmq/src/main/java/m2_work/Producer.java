package m2_work;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.MessageProperties;

import java.io.IOException;
import java.util.Scanner;
import java.util.concurrent.TimeoutException;

public class Producer {
    public static void main(String[] args) throws IOException, TimeoutException {
        //连接
        ConnectionFactory f = new ConnectionFactory();
        f.setHost("192.168.64.151");
        // f.setPort(5672);
        f.setUsername("admin");
        f.setPassword("admin");
        // f.setVirtualHost("/wht");

        Channel c = f.newConnection().createChannel();

        //定义队列
        c.queueDeclare("task_queue", true, false, false, null);

        //发消息
        while (true) {
            System.out.print("输入消息：");
            String msg = new Scanner(System.in).nextLine();
            c.basicPublish("", "task_queue", MessageProperties.PERSISTENT_TEXT_PLAIN, msg.getBytes());
        }
    }
}
